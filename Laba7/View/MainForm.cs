﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Laba7
{
    public partial class MainForm : Form
    {
        private BindingSource _bsStudents, _bsGroups;
        private ToolStripMenuItem _byGroupMenuItem, _allMenuItem;

        private void updateBothTables()
        {
            updateStudentsTable();
            updateGroupsTable();
        } 

        private void updateStudentsTable()
        {
            _bsStudents.DataSource = null;
            _bsStudents.DataSource
                = Model.Storage.GetStudentsForView();
            _bsStudents.ResetBindings(true);
        }

        private void updateGroupsTable()
        {
            _bsGroups.DataSource = null;
            _bsGroups.DataSource = Model.Storage.Instance.Groups;
            _bsGroups.ResetBindings(true);
        }

        private DialogResult _openInTheMiddle(Form form)
        {
            form.StartPosition = FormStartPosition.CenterParent;
            DialogResult result = form.ShowDialog(this);
            if (DialogResult.OK == result)
            {
                if (form is View.AddStudentForm
                        || form is View.InputNumberForm)
                {
                    updateStudentsTable();
                }
                else if (form is View.AddGroupForm)
                {
                    updateGroupsTable();
                }
            }
            return result;
        }
        
        public MainForm()
        {
            InitializeComponent();

            Text = "Laba7";
            
            _bsStudents = new BindingSource();
            _bsGroups = new BindingSource();
            
            var temp = new Dictionary<int, string>();
            temp[1] = "Add new item or open file with data!";
            _bsStudents.DataSource = temp; 
            _bsGroups.DataSource = temp;
            
            dgvStudents.DataSource = _bsStudents;
            dgvGroups.DataSource = _bsGroups;

            btnAddStudent.Click += (object sender, EventArgs args) => {
                _openInTheMiddle(new View.AddStudentForm());
            };
            btnAddGroup.Click += BtnAddGroup_Click;

            var filterItem = new ToolStripMenuItem("Filter for students");
            _byGroupMenuItem = new ToolStripMenuItem("By group number");
            _byGroupMenuItem.Click += ByGroupItem_Click;
            _allMenuItem = new ToolStripMenuItem("All students");
            _allMenuItem.Checked = true;
            filterItem.DropDownItems.AddRange( new ToolStripItem[] {
                _byGroupMenuItem, _allMenuItem
            });
            menuStrip1.Items.Add(filterItem);

            btnChange.Click += BtnChange_Click;

            _allMenuItem.Click += (object sender, EventArgs e) => {
                Model.DisplayMode.Filter = Model.Filter.All;
                updateStudentsTable();
                _allMenuItem.Checked = true;
                _byGroupMenuItem.Checked = false;
            };

            btnRemStud.Click += BtnRemStud_Click;

            var sortMenuItem = new ToolStripMenuItem("Sort students");
            menuStrip1.Items.Add(sortMenuItem);
            var bySurname = new ToolStripMenuItem("By surname");
            sortMenuItem.DropDownItems.Add(bySurname);
            var byGroup = new ToolStripMenuItem("By group");
            sortMenuItem.DropDownItems.Add(byGroup);

            bySurname.Click += (object sender, EventArgs e) =>
            {
                Model.DisplayMode.Criterion = Model.Sort.BySurname;
                updateStudentsTable();
                bySurname.Checked = true;
                byGroup.Checked = false;
            };

            byGroup.Click += (object sender, EventArgs e) =>
            {
                Model.DisplayMode.Criterion = Model.Sort.ByGroup;
                updateStudentsTable();
                bySurname.Checked = false;
                byGroup.Checked = true;
            };

            btnAccept.Click += BtnAccept_Click;

            Model.DBManager.LoadDataFromBD();
            updateBothTables();

            tsmiSave.Click += (s, e) =>
            {
                Model.DBManager.SaveData();
                MessageBox.Show("Data successfully saved!", "Done");
            };
            FormClosing += (s, e) => Model.DBManager.SaveData();
        }

        private void BtnAccept_Click(object sender, EventArgs e)
        {
            var studRows = dgvStudents.SelectedRows;
            if (0 == studRows.Count)
            {
                MessageBox.Show(this, "Select student to accept in some group!",
                    "Select student", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var groupRows = dgvGroups.SelectedRows;
            if (0 == groupRows.Count)
            {
                MessageBox.Show(this, "Select any group to accept student/students!",
                    "Select student", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } else if (1 < groupRows.Count) {
                MessageBox.Show(this, "Select only one group, please!",
                    "Select only one group, please!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            List<uint> ids = new List<uint>(studRows.Count);
            for (int i = 0; i < studRows.Count; ++i)
            {
                uint val = 0;
                string str_val = dgvStudents.Rows[studRows[i].Index].Cells[0].Value.ToString();
                try
                {
                    val = Convert.ToUInt32(str_val);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, ex.Message,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
                ids.Add(val);
            }

            uint grId = 0;
            string strGrId = dgvGroups.Rows[groupRows[0].Index].Cells[0].Value.ToString();
            try
            {
                grId = Convert.ToUInt32(strGrId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Model.Storage.ChangeGroupForStudents(ids, grId);

            updateStudentsTable();
        }

        private void BtnRemStud_Click(object sender, EventArgs e)
        {
            var rows = dgvStudents.SelectedRows;
            if (0 == rows.Count)
            {
                MessageBox.Show(this, "Select row you want to remove!",
                    "Select student", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            List<uint> ids = new List<uint>(rows.Count);
            for (int i = 0; i < rows.Count; ++i)
            {
                uint val = 0;
                string str_val = dgvStudents.Rows[rows[i].Index].Cells[0].Value.ToString();
                try
                {
                    val = Convert.ToUInt32(str_val);
                } catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, ex.Message,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
                ids.Add(val);
            }
            Model.Storage.DeleteStudentsDyId(ids);
            updateStudentsTable();
        }

        private void BtnAddGroup_Click(object sender, EventArgs e)
        {
            var form = new View.AddGroupForm();
            if (DialogResult.OK == _openInTheMiddle(form))
            {
                try
                {
                    Model.Storage.AddGroup(form.ResultGroup.Number, form.ResultGroup.Name);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, ex.Message, ex.Message,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                updateGroupsTable();
            }
        }

        private void BtnChange_Click(object sender, EventArgs e)
        {
            var inpNumForm = new View.InputNumberForm();
            inpNumForm.SetTitle("Input number of the grop to edit")
                .SetExplanation("For group with this number will " +
                "\nbe opened editing window.");
            if (DialogResult.OK == _openInTheMiddle(inpNumForm))
            {
                var form = new View.AddGroupForm();
                uint oldKey = Model.DisplayMode.ChosenGroupNumber;
                form.SetTitle("Group editing window")
                    .SetBtnText("Change")
                    .SetValues(Model.Storage.GetGroupById(oldKey));
                
                DialogResult res = _openInTheMiddle(form);
                if (res == DialogResult.OK)
                {
                    Model.Storage.ChangeGroup(form.ResultGroup, oldKey);
                    updateBothTables();
                }

            }
        }

        private void ByGroupItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _openInTheMiddle(new View.InputNumberForm()))
            {
                Model.DisplayMode.Filter = Model.Filter.ByGroupNumber;
                updateStudentsTable();
                _byGroupMenuItem.Checked = true;
                _allMenuItem.Checked = false;
            }
        }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Laba7.Model
{
    static class DBManager
    {
        private static readonly string DB_NAME = "Laba7_DB";

        /*Создание строки подключения
         Data Source - имя сервера, по стандарту (local)\SQLEXPRESS
        Initial Catalog - имя БД 
        Integrated Security= - параметры безопасности */
        private static readonly string _connStr = @"Data Source=(local)\SQLEXPRESS;"
                                                + $"Initial Catalog={DB_NAME};"
                                                + "Integrated Security=True";

        private static void myError(string message)
        {
            MessageBox.Show(message, "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void getConnection()
        {

        }

        //Method returns boolean value --- if database was existing before
        private static bool createDB_ifDoesntExist()
        {
            bool result;
            /*Здесь указано имя БД (для создания БД указывать не нужно)
              для проверки того, создана ли данная БД
            Создаем SqlConnection и передаем ему строку подключения */
            SqlConnection conn = new SqlConnection(_connStr);
            try
            {
                //попытка подключения
                conn.Open();
                result = true;
            }
            catch (SqlException se)
            {
                result = false;
                // Если база не обнаружена, то создаем новую
                if (se.Number == 4060)
                {
                    MessageBox.Show("Не было найдено БД, поэтому таблицы будут пустыми.");
                    //закрытие соединения
                    conn.Close();
                    //переопредение обьекта с новой строкой подключения
                    conn = new SqlConnection(@"Data Source=(local)\SQLEXPRESS;
                                                   Integrated Security=True");
                    /*Создание SqlCommand и передача запроса на создание БД
                     и объект типа SqlConnection */
                    var cmdCreateDataBase =
            new SqlCommand(string.Format("CREATE DATABASE [{0}]", DB_NAME), conn);
                    //открываем подключение
                    conn.Open();
                    /*Посылаем запрос к СУБД: в данном случае, в результате
			запроса ничего не возвращается */
                    Console.WriteLine("Посылаем запрос CREATE DB...");
                    cmdCreateDataBase.ExecuteNonQuery();
                    //закрываем подключение
                    conn.Close();
                    conn = new SqlConnection(_connStr);
                    //открываем подключение
                    conn.Open();
                }
            }
            finally
            {
                Console.WriteLine("Соедение успешно произведено");
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        private static void addTextParam(SqlCommand cmd, string paramName, string paramValue)
        {
            var param = new SqlParameter();
            param.ParameterName = paramName;

            if (null == paramValue)
            {
                param.Value = DBNull.Value;
            } else
            {
                param.Value = paramValue;
            }

            param.SqlDbType = SqlDbType.Text;
            cmd.Parameters.Add(param);
        }


        public static void SaveData()
        {
            var conn = new SqlConnection(_connStr);
            conn.Open();

            var sqlCmd = new SqlCommand("", conn);

            sqlCmd.CommandText = "DROP TABLE IF EXISTS groups;";
            sqlCmd.ExecuteNonQuery();
            sqlCmd.CommandText =
                @"CREATE TABLE groups(
                            id INT,
		                    name VARCHAR(100)
                        );";
            sqlCmd.ExecuteNonQuery();

            sqlCmd.CommandText = "DROP TABLE IF EXISTS students;";
            sqlCmd.ExecuteNonQuery();
            sqlCmd.CommandText =
                @"create table students(
                            id INT,
		                    surname VARCHAR(100),
		                    stud_name VARCHAR(100),
		                    phone_number VARCHAR(100),
		                    group_id INT
                        );";
            sqlCmd.ExecuteNonQuery();

            string insertGroupQuery = 
                @"INSERT INTO groups(id, name) 
                    VALUES (@ID, @NAME)";

            foreach (var group in Storage.Instance.Groups)
            {

                sqlCmd = new SqlCommand(insertGroupQuery, conn);

                /*Работа с параметрами(SqlParameter), эта техника позволяет 
            уменьшить количество ошибок и повысить быстродействие, но 
            требует и больших усилий в написании кода */
                //объявляем объект класса SqlParameter
                var param = new SqlParameter();
                
                //имя параметра
                param.ParameterName = "@ID";
                //значение параметра
                param.Value = group.Key;
                //тип параметра
                param.SqlDbType = SqlDbType.Int;
                //параметр объекту класса SqlCommand
                sqlCmd.Parameters.Add(param);

                addTextParam(sqlCmd, "@NAME", group.Value);

                sqlCmd.ExecuteNonQuery();
            }

            string insertStudentsQuery =
                @"INSERT INTO students(id, surname, stud_name, phone_number, group_id) 
                    VALUES (@ID, @SURNAME, @STUD_NAME, @PHONE_NUMBER, @GROUP_ID)";

            foreach (var stud in Storage.Instance.Students)
            {

                sqlCmd = new SqlCommand(insertStudentsQuery, conn);

                var param = new SqlParameter();
                param.ParameterName = "@ID";
                param.Value = stud.ID;
                param.SqlDbType = SqlDbType.Int;
                sqlCmd.Parameters.Add(param);

                addTextParam(sqlCmd, "@SURNAME", stud.Surname);
                addTextParam(sqlCmd, "@STUD_NAME", stud.Name);
                addTextParam(sqlCmd, "@PHONE_NUMBER", stud.phoneNumber);

                param = new SqlParameter();
                param.ParameterName = "@GROUP_ID";
                param.Value = (int) stud.GroupNumber;
                param.SqlDbType = SqlDbType.Int;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();
            }

            conn.Close();
            conn.Dispose();
        }

        public static void LoadDataFromBD()
        {
            if ( createDB_ifDoesntExist() )
            {
                var conn = new SqlConnection(_connStr);
                conn.Open();

                /*Создаем экземпляр класса SqlCommand и передаем запрос на 
                получение строк таблицы Students и объект типа SqlConnection */
                var cmd = new SqlCommand("SELECT * From groups", conn);
                
                /*Метод ExecuteReader() класса SqlCommand возвращает объект типа 
                  SqlDataReader, с помощью которого происходит чтение всех строк, 
               возврашенных в результате выполнения запроса
                  CommandBehavior.CloseConnection - закрытие соединения после */
                using (SqlDataReader dr =
                                cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        /*Метод GetValue() класса позволяет получить значение столбца
                         по номеру, который передается в качестве параметра и означает 
        номер столбца в таблице(начинается с 0) */
                        uint key = Convert.ToUInt32(dr.GetInt32(0));
                        string value = dr.GetString(1);
                        Storage.Instance.Groups[key] = value;
                    }
                }
                
                cmd = new SqlCommand("SELECT * FROM students", conn);
                using (SqlDataReader dr =
                                cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dr.Read())
                    {
                        Student stud = null;
                        try
                        {
                            uint id = Convert.ToUInt32(dr.GetInt32(0));
                            uint group_id = Convert.ToUInt32(dr.GetInt32(4));
                            stud = new Student(dr.GetString(1), dr.GetString(2), group_id);
                            stud.ID = id;

                            if (!dr.IsDBNull(3))
                            {
                                stud.phoneNumber = dr.GetString(3);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.StackTrace); 

                        }

                        Storage.Instance.Students.AddLast(stud);
                    }
                }
                
                //закрываем соединение
                conn.Close();
                conn.Dispose();
            }
        }
    }
}

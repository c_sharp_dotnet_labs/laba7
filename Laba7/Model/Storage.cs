﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Laba7.Model
{
    public class Group
    {
        public uint Number { get; set; }
        public string Name { get; set; }
        public bool wasNumberChanged { get; set; }
    }

    class Storage
    {
        public static Storage Instance { get; set; }

        public LinkedList<Student> Students { get; set; }
        public Dictionary<uint, string> Groups { get; set; }

        private Storage()
        {
            Students = new LinkedList<Student>();
            Groups = new Dictionary<uint, string>();
        }

        static Storage()
        {
            if (null == Instance)
            {
                Instance = new Storage();
            }
        }

        public static void AddStudent(Student st)
        {
            if (Instance.Groups.ContainsKey(st.GroupNumber))
            {
                Instance.Students.AddLast(st);
            } else
            {
                throw new ArgumentException(Model.ErrorMessages.NO_SUCH_KEY);
            }
        }

        public static void AddGroup(uint number, string Name)
        {
            if (Instance.Groups.ContainsKey(number))
            {
                throw new ArgumentException(
                    ErrorMessages.NOT_UNIQUE_KEY);
            }
            Instance.Groups[number] = Name;
        }

        public static IEnumerable<Student> GetStudentsForView()
        {
            IEnumerable<Student> studForView = Instance.Students;
            if (Model.DisplayMode.Filter == Filter.ByGroupNumber)
            {
                studForView = from Student st in Instance.Students
                              where st.GroupNumber == DisplayMode.ChosenGroupNumber
                              select st;
            }
            List<Student> sortedList = new List<Student>(studForView);
            if (Model.DisplayMode.Criterion == Sort.ByGroup)
            {
                sortedList.Sort(Student.ComparisonByGroup);
            } else
            {
                sortedList.Sort(Student.ComparisonBySurname);
            }
            return sortedList;
        }

        public static void ChangeGroup(Group group, uint oldKey = 0)
        {
            var groups = Instance.Groups;
            uint newKey = group.Number;
            groups[newKey] = group.Name;

            if (oldKey != 0 && newKey != oldKey)
            {
                groups.Remove(oldKey);
                LinkedList<Student>.Enumerator enumerator
                    = Instance.Students.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.GroupNumber == oldKey)
                    {
                        enumerator.Current.GroupNumber = newKey; 
                    }
                }
            }
        }

        public static Group GetGroupById(uint key)
        {
            return new Group() { 
                Number = key, 
                Name = Instance.Groups[key] 
            };
        }

        public static void DeleteStudentsDyId(IEnumerable<uint> ids)
        {
            var currentNode = Instance.Students.First;
            while (null != currentNode)
            {
                foreach (uint id in ids)
                {
                    if (id == currentNode.Value.ID)
                    {
                        var toRemove = currentNode;
                        currentNode = currentNode.Next;
                        Instance.Students.Remove(toRemove);
                    } else
                    {
                        currentNode = currentNode.Next;
                    }
                }
            }
        }

        public static void ChangeGroupForStudents(IEnumerable<uint> studIds, uint newGroupKey)
        {
            var currentNode = Instance.Students.First;
            while (null != currentNode)
            {
                foreach (uint id in studIds)
                {
                    if (id == currentNode.Value.ID)
                    {
                        currentNode.Value.GroupNumber = newGroupKey;
                    }
                }
                currentNode = currentNode.Next;
            }
        }

    }
}
